CREATE VIEW ManageEmployee AS
	SELECT
	e.EmployeeID,
	e.EmployeeName,
	e.EmployeeSurname,
	d.DeptName,
	m.ManagerName,
	m.ManagerSurname
FROM
	Employee e
	INNER JOIN
	Department d ON d.DeptID = e.DeptID
	INNER JOIN 
	Manager m ON m.DeptID = d.DeptID

